export const environment = {
  production: true,
    /**CONSTANTES */
    URL_LOGIN: 'https://ies-webcontent.com.mx/xccm/user/validarUsuario',
    URL_COUNTRY: 'https://api.first.org/data/v1/countries?region=africa&limit=10&pretty=true',
                  
    CONST_LOCALSTORAGE_SESION: 'ies'
};
