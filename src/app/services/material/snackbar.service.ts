import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(
    private _snackBar: MatSnackBar
  ) { }

  config:MatSnackBarConfig<any> = {
    horizontalPosition:'center',
    verticalPosition: 'bottom',
    duration: 3500
  }

  showAlert(message:string){
    this._snackBar.open(message, "cerrar",this.config);
  }
}
