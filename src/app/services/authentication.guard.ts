import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Roles } from '../enums/roles.enum';
import { SnackbarService } from './material/snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(
    private router: Router,
    private snacbarService: SnackbarService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const {resultado} =  JSON.parse(localStorage.getItem(environment.CONST_LOCALSTORAGE_SESION) || '{}');
      
      if(resultado){
        const {id_rol} = resultado;
        if(id_rol && id_rol == Roles.DISTRIBUIDOR){
          return true;
        }else{
          this.snacbarService.showAlert("No tienes permisos para acceder al sitio.");
          localStorage.removeItem(environment.CONST_LOCALSTORAGE_SESION);
        }
      }
  
      this.router.navigateByUrl('/login');
       return false;
  }
  
}
