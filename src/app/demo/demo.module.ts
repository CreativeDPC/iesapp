import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ComponenteUnoComponent } from './componente-uno/componente-uno.component';
import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NavComponent } from '../components/nav/nav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpService } from '../services/http.service';

@NgModule({
  declarations: [
    DashboardComponent, 
    ComponenteUnoComponent,
    DemoComponent,
    NavComponent
  ],
  imports: [
    CommonModule,
    DemoRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[HttpService]
})
export class DemoModule { }
