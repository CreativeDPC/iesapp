import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.scss']
})
export class ComponenteUnoComponent implements OnInit {

  data = [ 
    { value: 1 , name: 'CampoUno'}, 
    { value: 2 , name: 'CampoDos'}, 
    { value: 3 , name: 'CampoTres'}, 
    { value: 4 , name: 'CampoCuatro'}, 
    { value: 5 , name: 'CampoCinco'}, 
    { value: 6 , name: 'CampoSeis'} 
  ];

  data2 = { CampoUno: 1, CampoDos: 2, CampoTres: 3, CampoCuatro: 4, CampoCinco: 5, CampoSeis: 6 };

  in:string = JSON.stringify(this.data);
  out:string = "";
  reverse:boolean = false;

  constructor() { }

  ngOnInit() {

  }

  procesar(){
    if(this.reverse){
      let array = [];
      for (const key in this.data2) {
        if (Object.prototype.hasOwnProperty.call(this.data2, key)) {
          array = [...array, {value: this.data2[key] , name: key}]
        }
      }

      this.out = JSON.stringify(array);
      
    }else{
      let objeto = {}
      this.data.forEach(e =>{
        objeto = {...objeto, [e.name]: e.value}
      });
      this.out = JSON.stringify(objeto);    
    }
  }

  change(isReverse){
    this.reverse = isReverse;
    if(isReverse){
      this.in = JSON.stringify(this.data2);
    }else{
      this.in = JSON.stringify(this.data);
    }
    this.out = '';
  }

}
