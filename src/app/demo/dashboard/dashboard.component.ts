import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  paises:Array<any> = [{name:"Pais de Prueba", value:"PD"}];

  dashForm =  new FormGroup({
    nombre: new FormControl('',[Validators.required]),
    email: new FormControl('',[Validators.required, Validators.email]),
    fechaReserva: new FormControl('',[Validators.required]),
    pais: new FormControl('')
  });

  constructor(
    private httpService:HttpService
  ) { }

  ngOnInit() {
    this.httpService.get(environment.URL_COUNTRY)
    .subscribe({
      next: res =>{
        if(res?.status == 'OK'){
          const {data} = res;
          for (const key in data) {
            if (Object.prototype.hasOwnProperty.call(data, key)) {
              this.paises = [...this.paises, {name: data[key]?.country, value: key}];
            }
          }
        }
      }
    });
  }
  onSubmit(){
    console.log("RESULTADO", this.dashForm.value);
  }

  getPaises(){
    
  }

  dateFilter = (d: Date | null): boolean => {
    const date_picker = (d || new Date());
    const date_now = new Date(Date.now());
    const date_tomorrow = new Date(
      date_now.getFullYear(),
      date_now.getMonth(), 
      date_now.getDate() + 2
    );

    date_now.setMonth(date_now.getMonth() - 11);

    return date_picker > date_now && date_tomorrow > date_picker;
  }

}
