import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponenteUnoComponent } from './componente-uno/componente-uno.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoComponent } from './demo.component';


const routes: Routes = [
  {
    path: '',
    component: DemoComponent,
    children:[
      {
        path: '',
        component: ComponenteUnoComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      }      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule { }
