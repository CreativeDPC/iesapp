import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthenticationGuard } from './services/authentication.guard';
import { HttpService } from './services/http.service';

const routes: Routes = [
  {
    component: LoginComponent,
    path:'login'
  },
  {
    path:'',
    canActivate: [AuthenticationGuard],
    loadChildren: () =>
    import(`./demo/demo.module`).then(
      (m) => m.DemoModule
    )    
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/'
  }
  ,
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[HttpService]
})
export class AppRoutingModule { }
