import { SnackbarService } from './../services/material/snackbar.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private httpservices:HttpService,
    private router: Router,
    private snacbarService: SnackbarService
  ) { }

  loginForm = new FormGroup({
    user: new FormControl('',[Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required,  Validators.minLength(3)]),
  });

  ngOnInit() {

  }

  onSubmit(){

    this.httpservices.post(environment.URL_LOGIN,{"usuario": this.loginForm.get('user').value, "contrasena": this.loginForm.get('password').value})
    .subscribe({
      next: res =>{
        if(res) {
          if(res.resultado){
            localStorage.setItem(environment.CONST_LOCALSTORAGE_SESION, JSON.stringify(res));
            this.router.navigate(['/']);
          }else{
            this.snacbarService.showAlert("Usuario o contraseña incorrectos");
          }
        }
        
      },
      error: err => {
        console.error("Err",err);
      }
    });
    
  }

}
